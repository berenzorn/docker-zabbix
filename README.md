# docker-zabbix

```
apt install -y docker.io
systemctl start docker
```

## for ubuntu 18.04
```
apt-get install -y dnsmasq resolvconf
echo "interface=docker0" >> /etc/dnsmasq.conf
echo "bind-interfaces" >> /etc/dnsmasq.conf
echo "listen-address=172.17.0.1" >> /etc/dnsmasq.conf
echo "nameserver 172.17.0.1" >> /etc/resolvconf/resolv.conf.d/tail
service network-manager restart
resolvconf -u
systemctl restart docker
service dnsmasq restart
```


- portainer port 9000
- mariadb
- zabbix server
- zabbix web iface
- zabbix agent
```
docker run -d -p 9000:9000 -p 8000:8000 -v /var/run/docker.sock:/var/run/docker.sock -v /opt/portainer:/data portainer/portainer
docker run --name mariadb -e MYSQL_USER="zabbix" -e MYSQL_PASSWORD="6ntqkas7" -e MYSQL_ROOT_PASSWORD="fmqdm8ybzpa7" -d mariadb:latest
docker run --name zabbix-server --link mariadb:mariadb -p 10051:10051 -e DB_SERVER_HOST="mariadb" -e MYSQL_ROOT_PASSWORD="fmqdm8ybzpa7" -d zabbix/zabbix-server-mysql:ubuntu-latest
docker run --name zabbix-web --link mariadb:mariadb --link zabbix-server:zabbix-server-mysql -p 80:8080 -e DB_SERVER_HOST="mariadb" -e MYSQL_ROOT_PASSWORD="fmqdm8ybzpa7" -e ZBX_SERVER_HOST="zabbix-server" -e PHP_TZ=Europe/Moscow -d zabbix/zabbix-web-nginx-mysql:ubuntu-latest
docker run --name zagent --link zabbix-server:zabbix-server-mysql -d zabbix/zabbix-agent:ubuntu-latest
```

### postgres вариант, версия 4.0 latest
```
docker run -d -p 9000:9000 -p 8000:8000 -v /var/run/docker.sock:/var/run/docker.sock -v /opt/portainer:/data portainer/portainer
docker run --name postgres -e POSTGRES_PASSWORD="fmqdm8ybzpa7" -e POSTGRES_USER="zabbix" -e POSTGRES_DB="zabbix" -d postgres
docker run --name zabbix-server --link postgres -e DB_SERVER_HOST="postgres" -e POSTGRES_USER="zabbix" -e POSTGRES_PASSWORD="fmqdm8ybzpa7" -d zabbix/zabbix-server-pgsql:ubuntu-4.0-latest
docker run --name java-gateway --link zabbix-server -d zabbix/zabbix-java-gateway:ubuntu-4.0-latest
docker run --name zabbix-web -p 8080:8080 --link zabbix-server --link postgres -e DB_SERVER_HOST="postgres" -e POSTGRES_USER="zabbix" -e POSTGRES_PASSWORD="fmqdm8ybzpa7" -e ZBX_SERVER_HOST="zabbix-server" -e PHP_TZ="Europe/Moscow" -d zabbix/zabbix-web-nginx-pgsql:ubuntu-4.0-latest
docker run --name zabbix-agent --link zabbix-server -e ZBX_HOSTNAME="Zabbix server" -e ZBX_SERVER_HOST="zabbix-server" -P -d zabbix/zabbix-agent:ubuntu-4.0-latest
```

### рабочий вариант с appliance
```
docker run -d -p 9000:9000 -p 8000:8000 -v /var/run/docker.sock:/var/run/docker.sock -v /opt/portainer:/data portainer/portainer
docker run --name zabbix-server -p 80:80 -p 10051:10051 -d zabbix/zabbix-appliance:latest
docker run --name zabbix-agent --link zabbix-server:zabbix-server -d zabbix/zabbix-agent:latest
```

### ещё один вариант
```
docker run -d -p 9000:9000 -p 8000:8000 -v /var/run/docker.sock:/var/run/docker.sock -v /opt/portainer:/data portainer/portainer
docker run --name mysql -t -e MYSQL_DATABASE="zabbix" -e MYSQL_USER="zabbix" -e MYSQL_PASSWORD="6ntqkas7" -e MYSQL_ROOT_PASSWORD="fmqdm8ybzpa7" -d mysql:latest
docker run --name zabbix-java-gateway -t -d zabbix/zabbix-java-gateway
docker run --name zabbix-server-mysql -t -e DB_SERVER_HOST="mysql" -e MYSQL_DATABASE="zabbix" -e MYSQL_USER="zabbix" -e MYSQL_PASSWORD="6ntqkas7" -e MYSQL_ROOT_PASSWORD="fmqdm8ybzpa7" -e ZBX_JAVAGATEWAY="zabbix-java-gateway" --link mysql:mysql --link zabbix-java-gateway:zabbix-java-gateway -p 10051:10051 -d zabbix/zabbix-server-mysql
docker run --name zabbix-web-nginx-mysql -t -e DB_SERVER_HOST="mysql" -e MYSQL_DATABASE="zabbix" -e MYSQL_USER="zabbix" -e MYSQL_PASSWORD="6ntqkas7" -e MYSQL_ROOT_PASSWORD="fmqdm8ybzpa7" --link mysql:mysql --link zabbix-server:zabbix-server-mysql -p 80:8080 -d zabbix/zabbix-web-nginx-mysql
docker run --name zabbix-agent --link mysql:mysql --link zabbix-server:zabbix-server-mysql -e ZBX_HOSTNAME="Zabbix server" -e ZBX_SERVER_HOST="zabbix-server" -d zabbix/zabbix-agent
docker inspect zabbix-agent | grep "IPAddress\": " && mkdir /var/lib/grafana -p && chown -R 472:472 /var/lib/grafana
docker run -d -p 3000:3000 -v /var/lib/grafana:/var/lib/grafana -e "GF_SECURITY_ADMIN_PASSWORD=6ntqkas7" grafana/grafana
```

